package com.thoughtworks.collection;

import java.util.List;

public class StreamReduce {

    public int getLastOdd(List<Integer> numbers) {

        return null;
    }

    public String getLongest(List<String> words) {
        return null;
    }

    public int getTotalLength(List<String> words) {

//        return words.stream().map(String::length).reduce(0,Integer::sum);
    return words.stream().reduce(0, (initValue,word) -> initValue + word.length(), Integer::sum);
    }

}
